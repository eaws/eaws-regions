import fs from "fs";
import path from "path";
import filterFeature from "./filterFeature.mjs";

build();

function build(dir = "public") {
  fs.readdirSync(dir, { withFileTypes: true })
    .filter((d) => d.isDirectory())
    .filter((d) => d.name !== "micro-regions_properties")
    .map((d) => path.join(dir, d.name))
    .sort()
    .forEach((d) => buildLatest(d));
}

/**
 * @param {string} dir
 */
function buildLatest(dir) {
  /**
   * @type {import('geojson').FeatureCollection}
   */
  const allFeatures = { type: "FeatureCollection", features: [] };
  glob(dir, /.geojson.json$/i).forEach((f) => {
    /**
     * @type {import('geojson').FeatureCollection}
     */
    const geojson = JSON.parse(fs.readFileSync(f, { encoding: "utf8" }));
    console.log("Reading %d features from %s", geojson.features.length, f);

    if (
      ["micro-regions", "micro-regions_elevation"].includes(path.basename(dir))
    ) {
      const file = f
        .replace(dir, dir + "_properties")
        .replace(/geojson.json$/, "json");
      console.log("Writing %s", file);
      fs.writeFileSync(
        file,
        JSON.stringify(
          geojson.features.map((f) => f.properties),
          null,
          2
        ),
        { encoding: "utf8" }
      );
    }

    allFeatures.features.push(...geojson.features);

    if (["outline", "latest"].includes(path.basename(dir))) {
      return;
    }

    geojson.features = geojson.features.filter((feature) =>
      filterFeature(feature)
    );

    const latest = path.join(path.dirname(f), "latest", path.basename(f));
    fs.mkdirSync(path.dirname(latest), { recursive: true });
    fs.writeFileSync(latest, JSON.stringify(geojson), { encoding: "utf8" });
    console.log("Writing %d features to %s", geojson.features.length, latest);
  });
  if (!allFeatures.features.length) {
    return;
  }
  const all = path.join(dir, "..", path.basename(dir) + ".geojson");
  fs.writeFileSync(all, JSON.stringify(allFeatures), { encoding: "utf8" });
  console.log("Writing %d features to %s", allFeatures.features.length, all);
  if ("outline" == path.basename(dir)) {
    buildCombinedOutline(dir, allFeatures);
  }
}

/**
 * @param {string} dir
 * @param {import('geojson').FeatureCollection<Geometry, GeoJsonProperties>} allFeatures
 */
function buildCombinedOutline(dir, allFeatures) {
  const allProperties = allFeatures.features
    .map((f) => f.properties)
    .filter((p, idx, arr) => idx === arr.findIndex((p0) => p.id === p0.id));
  [
    path.join(dir, "..", path.basename(dir) + ".json"),
    path.join(dir, "..", path.basename(dir) + "_properties/index.json"),
  ].forEach((all) => {
    fs.writeFileSync(all, JSON.stringify(allProperties, null, 2), {
      encoding: "utf8",
    });
    console.log(
      "Writing %d feature properties to %s",
      allFeatures.features.length,
      all
    );
  });
}

function glob(dir, regex) {
  return fs
    .readdirSync(dir, { withFileTypes: true })
    .filter((f) => f.isFile() && regex.test(f.name))
    .map((f) => path.join(dir, f.name))
    .sort();
}
